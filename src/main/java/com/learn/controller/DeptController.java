package com.learn.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.learn.entity.DeptEntity;
import com.learn.service.DeptService;
import com.learn.utils.PageUtils;
import com.learn.utils.Query;
import com.learn.utils.R;


/**
 * 参赛单位
 * 
 * 
 * 
 */
@RestController
@RequestMapping("dept")
public class DeptController extends AbstractController {
	@Autowired
	private DeptService deptService;
	
	/**
	 * 列表
	 */
	@RequestMapping("/list")
	public R list(@RequestParam Map<String, Object> params){

																											

		//查询列表数据
        Query query = new Query(params);

		List<DeptEntity> deptList = deptService.queryList(query);
		int total = deptService.queryTotal(query);
		
		PageUtils pageUtil = new PageUtils(deptList, total, query.getLimit(), query.getPage());
		
		return R.ok().put("page", pageUtil);
	}
	

	/**
	 * 列表
	 */
	@RequestMapping("/list2")
	public R list2(@RequestParam Map<String, Object> params){
        Query query = new Query(params);
		List<DeptEntity> deptList = deptService.queryList(query);
		return R.ok().put("list", deptList );
	}


	/**
	 * 信息
	 */
	@RequestMapping("/info/{id}")
	public R info(@PathVariable("id") Long id){
		DeptEntity dept = deptService.queryObject(id);
		
		return R.ok().put("dept", dept);
	}
	
	/**
	 * 保存
	 */
	@RequestMapping("/save")
	public R save(@RequestBody DeptEntity dept){

																											

        deptService.save(dept);
		
		return R.ok();
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	public R update(@RequestBody DeptEntity dept){
		deptService.update(dept);
		
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	public R delete(@RequestBody Long[] ids){
		deptService.deleteBatch(ids);
		
		return R.ok();
	}
	
}
