package com.learn.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.learn.entity.SportEntity;
import com.learn.service.SportService;
import com.learn.utils.PageUtils;
import com.learn.utils.Query;
import com.learn.utils.R;


/**
 * 运动员信息
 * 
 * 
 * 
 */
@RestController
@RequestMapping("sport")
public class SportController extends AbstractController {
	@Autowired
	private SportService sportService;
	
	/**
	 * 列表
	 */
	@RequestMapping("/list")
	public R list(@RequestParam Map<String, Object> params){

																														                if (super.getUserId() > 1)
                    params.put("user", super.getUserId());
										

		//查询列表数据
        Query query = new Query(params);

		List<SportEntity> sportList = sportService.queryList(query);
		int total = sportService.queryTotal(query);
		
		PageUtils pageUtil = new PageUtils(sportList, total, query.getLimit(), query.getPage());
		
		return R.ok().put("page", pageUtil);
	}
	

	/**
	 * 列表
	 */
	@RequestMapping("/list2")
	public R list2(@RequestParam Map<String, Object> params){
        Query query = new Query(params);
		List<SportEntity> sportList = sportService.queryList(query);
		return R.ok().put("list", sportList );
	}


	/**
	 * 信息
	 */
	@RequestMapping("/info/{id}")
	public R info(@PathVariable("id") Long id){
		SportEntity sport = sportService.queryObject(id);
		
		return R.ok().put("sport", sport);
	}
	
	/**
	 * 保存
	 */
	@RequestMapping("/save")
	public R save(@RequestBody SportEntity sport){

																														                if (sport.getUser() == null)
                    sport.setUser(super.getUserId());
										

        sportService.save(sport);
		
		return R.ok();
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	public R update(@RequestBody SportEntity sport){
		sportService.update(sport);
		
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	public R delete(@RequestBody Long[] ids){
		sportService.deleteBatch(ids);
		
		return R.ok();
	}
	
}
