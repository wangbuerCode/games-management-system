package com.learn.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.learn.entity.SaiEntity;
import com.learn.service.SaiService;
import com.learn.utils.PageUtils;
import com.learn.utils.Query;
import com.learn.utils.R;


/**
 * 比赛赛程
 * 
 * 
 * 
 */
@RestController
@RequestMapping("sai")
public class SaiController extends AbstractController {
	@Autowired
	private SaiService saiService;
	
	/**
	 * 列表
	 */
	@RequestMapping("/list")
	public R list(@RequestParam Map<String, Object> params){

																																										

		//查询列表数据
        Query query = new Query(params);

		List<SaiEntity> saiList = saiService.queryList(query);
		int total = saiService.queryTotal(query);
		
		PageUtils pageUtil = new PageUtils(saiList, total, query.getLimit(), query.getPage());
		
		return R.ok().put("page", pageUtil);
	}
	

	/**
	 * 列表
	 */
	@RequestMapping("/list2")
	public R list2(@RequestParam Map<String, Object> params){
        Query query = new Query(params);
		List<SaiEntity> saiList = saiService.queryList(query);
		return R.ok().put("list", saiList );
	}


	/**
	 * 信息
	 */
	@RequestMapping("/info/{id}")
	public R info(@PathVariable("id") Long id){
		SaiEntity sai = saiService.queryObject(id);
		
		return R.ok().put("sai", sai);
	}
	
	/**
	 * 保存
	 */
	@RequestMapping("/save")
	public R save(@RequestBody SaiEntity sai){


        saiService.save(sai);
		
		return R.ok();
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	public R update(@RequestBody SaiEntity sai){
		saiService.update(sai);
		
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	public R delete(@RequestBody Long[] ids){
		saiService.deleteBatch(ids);
		
		return R.ok();
	}
	
}
