package com.learn.entity;

        import java.io.Serializable;
        import java.util.Date;
        import com.learn.service.*;



/**
 * 运动员信息
 *
 
 *  
 */
public class SportEntity implements Serializable {
    private static final long serialVersionUID = 1L;

            //
                    private Long id;
        
            //姓名
                    private String name;
        
            //编号
                    private String code;
        
            //联系方式
                    private String phone;
        
            //所属参数单位
                    private Long dept;

            private  DeptEntity  deptEntity;

            public DeptEntity getDeptEntity() {
                return deptEntity;
            }

            public void setDeptEntity(DeptEntity deptEntity) {
                this.deptEntity = deptEntity;
            }
        
            //绑定登录账号
                    private Long user;

            private SysUserEntity sysUserEntity;

            public SysUserEntity getSysUserEntity() {
                return sysUserEntity;
            }

            public void setSysUserEntity(SysUserEntity sysUserEntity) {
                this.sysUserEntity = sysUserEntity;
            }

        
            //添加时间
                    private Date gmttime =new  Date();
        
    
            /**
         * 设置：
         */
        public void setId(Long id) {
            this.id = id;
        }

        /**
         * 获取：
         */
        public Long getId() {
            return id;
        }
            /**
         * 设置：姓名
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * 获取：姓名
         */
        public String getName() {
            return name;
        }
            /**
         * 设置：编号
         */
        public void setCode(String code) {
            this.code = code;
        }

        /**
         * 获取：编号
         */
        public String getCode() {
            return code;
        }
            /**
         * 设置：联系方式
         */
        public void setPhone(String phone) {
            this.phone = phone;
        }

        /**
         * 获取：联系方式
         */
        public String getPhone() {
            return phone;
        }
            /**
         * 设置：所属参数单位
         */
        public void setDept(Long dept) {
            this.dept = dept;
        }

        /**
         * 获取：所属参数单位
         */
        public Long getDept() {
            return dept;
        }
            /**
         * 设置：绑定登录账号
         */
        public void setUser(Long user) {
            this.user = user;
        }

        /**
         * 获取：绑定登录账号
         */
        public Long getUser() {
            return user;
        }
            /**
         * 设置：添加时间
         */
        public void setGmttime(Date gmttime) {
            this.gmttime = gmttime;
        }

        /**
         * 获取：添加时间
         */
        public Date getGmttime() {
            return gmttime;
        }
    }
