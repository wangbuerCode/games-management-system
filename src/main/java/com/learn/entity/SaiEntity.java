package com.learn.entity;

        import java.io.Serializable;
        import java.util.Date;
        import com.learn.service.*;



/**
 * 比赛赛程
 *
 
 *  
 */
public class SaiEntity implements Serializable {
    private static final long serialVersionUID = 1L;

            //
                    private Long id;
        
            //名称
                    private String name;
        
            //所属别类
                    private Long type;

            private  TypeEntity  typeEntity;

            public TypeEntity getTypeEntity() {
                return typeEntity;
            }

            public void setTypeEntity(TypeEntity typeEntity) {
                this.typeEntity = typeEntity;
            }
        
            //比赛时间
                    private Date time;
        
            //入场时间
                    private Date begin;
        
            //裁判
                    private String cai;
        
            //特殊说明
                    private String remark;

                    private Double num;

    public Double getNum() {
        return num;
    }

    public void setNum(Double num) {
        this.num = num;
    }

    //添加时间
                    private Date gmttime =new  Date();
        
    
            /**
         * 设置：
         */
        public void setId(Long id) {
            this.id = id;
        }

        /**
         * 获取：
         */
        public Long getId() {
            return id;
        }
            /**
         * 设置：名称
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * 获取：名称
         */
        public String getName() {
            return name;
        }
            /**
         * 设置：所属别类
         */
        public void setType(Long type) {
            this.type = type;
        }

        /**
         * 获取：所属别类
         */
        public Long getType() {
            return type;
        }
            /**
         * 设置：比赛时间
         */
        public void setTime(Date time) {
            this.time = time;
        }

        /**
         * 获取：比赛时间
         */
        public Date getTime() {
            return time;
        }
            /**
         * 设置：入场时间
         */
        public void setBegin(Date begin) {
            this.begin = begin;
        }

        /**
         * 获取：入场时间
         */
        public Date getBegin() {
            return begin;
        }
            /**
         * 设置：裁判
         */
        public void setCai(String cai) {
            this.cai = cai;
        }

        /**
         * 获取：裁判
         */
        public String getCai() {
            return cai;
        }
            /**
         * 设置：特殊说明
         */
        public void setRemark(String remark) {
            this.remark = remark;
        }

        /**
         * 获取：特殊说明
         */
        public String getRemark() {
            return remark;
        }
            /**
         * 设置：添加时间
         */
        public void setGmttime(Date gmttime) {
            this.gmttime = gmttime;
        }

        /**
         * 获取：添加时间
         */
        public Date getGmttime() {
            return gmttime;
        }
    }
