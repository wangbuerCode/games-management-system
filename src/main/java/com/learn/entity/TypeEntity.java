package com.learn.entity;

        import java.io.Serializable;
        import java.util.Date;
        import com.learn.service.*;



/**
 * 比赛类别
 *
 
 *  
 */
public class TypeEntity implements Serializable {
    private static final long serialVersionUID = 1L;

            //
                    private Long id;
        
            //名称
                    private String name;
        
            //内容
                    private String content;
        
            //特殊说明
                    private String special;

                    private String sort;

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    //添加时间
                    private Date gmttime =new  Date();
        
    
            /**
         * 设置：
         */
        public void setId(Long id) {
            this.id = id;
        }

        /**
         * 获取：
         */
        public Long getId() {
            return id;
        }
            /**
         * 设置：名称
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * 获取：名称
         */
        public String getName() {
            return name;
        }
            /**
         * 设置：内容
         */
        public void setContent(String content) {
            this.content = content;
        }

        /**
         * 获取：内容
         */
        public String getContent() {
            return content;
        }
            /**
         * 设置：特殊说明
         */
        public void setSpecial(String special) {
            this.special = special;
        }

        /**
         * 获取：特殊说明
         */
        public String getSpecial() {
            return special;
        }
            /**
         * 设置：添加时间
         */
        public void setGmttime(Date gmttime) {
            this.gmttime = gmttime;
        }

        /**
         * 获取：添加时间
         */
        public Date getGmttime() {
            return gmttime;
        }
    }
