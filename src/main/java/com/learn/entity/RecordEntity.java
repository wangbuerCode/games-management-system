package com.learn.entity;

        import java.io.Serializable;
        import java.util.Date;
        import com.learn.service.*;



/**
 * 比赛安排
 *
 
 *  
 */
public class RecordEntity implements Serializable {
    private static final long serialVersionUID = 1L;

            //
                    private Long id;
        
            //比赛赛程
                    private Long sai;

            private  SaiEntity  saiEntity;

            public SaiEntity getSaiEntity() {
                return saiEntity;
            }

            public void setSaiEntity(SaiEntity saiEntity) {
                this.saiEntity = saiEntity;
            }
        
            //运动员
                    private Long sport;

            private  SportEntity  sportEntity;

            public SportEntity getSportEntity() {
                return sportEntity;
            }

            public void setSportEntity(SportEntity sportEntity) {
                this.sportEntity = sportEntity;
            }
        
            //账号
                    private Long user;

            private SysUserEntity sysUserEntity;

            public SysUserEntity getSysUserEntity() {
                return sysUserEntity;
            }

            public void setSysUserEntity(SysUserEntity sysUserEntity) {
                this.sysUserEntity = sysUserEntity;
            }

        
            //成绩
                    private Double score;
        
            //名次
                    private Double ming;
        
            //备注
                    private String remark;

                    private Long type;

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    //添加时间
                    private Date gmttime =new  Date();
        
    
            /**
         * 设置：
         */
        public void setId(Long id) {
            this.id = id;
        }

        /**
         * 获取：
         */
        public Long getId() {
            return id;
        }
            /**
         * 设置：比赛赛程
         */
        public void setSai(Long sai) {
            this.sai = sai;
        }

        /**
         * 获取：比赛赛程
         */
        public Long getSai() {
            return sai;
        }
            /**
         * 设置：运动员
         */
        public void setSport(Long sport) {
            this.sport = sport;
        }

        /**
         * 获取：运动员
         */
        public Long getSport() {
            return sport;
        }
            /**
         * 设置：账号
         */
        public void setUser(Long user) {
            this.user = user;
        }

        /**
         * 获取：账号
         */
        public Long getUser() {
            return user;
        }
            /**
         * 设置：成绩
         */
        public void setScore(Double score) {
            this.score = score;
        }

        /**
         * 获取：成绩
         */
        public Double getScore() {
            return score;
        }
            /**
         * 设置：名次
         */
        public void setMing(Double ming) {
            this.ming = ming;
        }

        /**
         * 获取：名次
         */
        public Double getMing() {
            return ming;
        }
            /**
         * 设置：备注
         */
        public void setRemark(String remark) {
            this.remark = remark;
        }

        /**
         * 获取：备注
         */
        public String getRemark() {
            return remark;
        }
            /**
         * 设置：添加时间
         */
        public void setGmttime(Date gmttime) {
            this.gmttime = gmttime;
        }

        /**
         * 获取：添加时间
         */
        public Date getGmttime() {
            return gmttime;
        }
    }
