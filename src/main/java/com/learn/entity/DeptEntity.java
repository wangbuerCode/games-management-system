package com.learn.entity;

        import java.io.Serializable;
        import java.util.Date;
        import com.learn.service.*;



/**
 * 参赛单位
 *
 
 *  
 */
public class DeptEntity implements Serializable {
    private static final long serialVersionUID = 1L;

            //
                    private Long id;
        
            //名称
                    private String name;
        
            //负责人
                    private String fzr;
        
            //联系电话
                    private String phone;
        
            //添加时间
                    private Date gmttime =new  Date();
        
    
            /**
         * 设置：
         */
        public void setId(Long id) {
            this.id = id;
        }

        /**
         * 获取：
         */
        public Long getId() {
            return id;
        }
            /**
         * 设置：名称
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * 获取：名称
         */
        public String getName() {
            return name;
        }
            /**
         * 设置：负责人
         */
        public void setFzr(String fzr) {
            this.fzr = fzr;
        }

        /**
         * 获取：负责人
         */
        public String getFzr() {
            return fzr;
        }
            /**
         * 设置：联系电话
         */
        public void setPhone(String phone) {
            this.phone = phone;
        }

        /**
         * 获取：联系电话
         */
        public String getPhone() {
            return phone;
        }
            /**
         * 设置：添加时间
         */
        public void setGmttime(Date gmttime) {
            this.gmttime = gmttime;
        }

        /**
         * 获取：添加时间
         */
        public Date getGmttime() {
            return gmttime;
        }
    }
