package com.learn.dao;

import com.learn.entity.TypeEntity;

/**
 * 比赛类别
 * 
 
 *  
 */
public interface TypeDao extends BaseDao<TypeEntity> {
	
}
