package com.learn.dao;

import com.learn.entity.SportEntity;

/**
 * 运动员信息
 * 
 
 *  
 */
public interface SportDao extends BaseDao<SportEntity> {
	
}
