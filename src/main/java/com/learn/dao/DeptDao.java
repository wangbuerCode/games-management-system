package com.learn.dao;

import com.learn.entity.DeptEntity;

/**
 * 参赛单位
 * 
 
 *  
 */
public interface DeptDao extends BaseDao<DeptEntity> {
	
}
