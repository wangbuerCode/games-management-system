package com.learn.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

					

import com.learn.dao.DeptDao;
import com.learn.entity.DeptEntity;
import com.learn.service.DeptService;
import com.learn.service.*;



@Service("deptService")
public class DeptServiceImpl implements DeptService {
	@Autowired
	private DeptDao deptDao;

			

			

			

			

			

	

	
	@Override
	public DeptEntity queryObject(Long id){
			DeptEntity entity = deptDao.queryObject(id);

																
		return entity;
	}
	
	@Override
	public List<DeptEntity> queryList(Map<String, Object> map){
        List<DeptEntity> list = deptDao.queryList(map);
        for(DeptEntity entity : list){
																																								}
		return list;
	}
	
	@Override
	public int queryTotal(Map<String, Object> map){
		return deptDao.queryTotal(map);
	}
	
	@Override
	public void save(DeptEntity dept){
		deptDao.save(dept);
	}
	
	@Override
	public void update(DeptEntity dept){
		deptDao.update(dept);
	}
	
	@Override
	public void delete(Long id){
		deptDao.delete(id);
	}
	
	@Override
	public void deleteBatch(Long[] ids){
		deptDao.deleteBatch(ids);
	}
	
}
