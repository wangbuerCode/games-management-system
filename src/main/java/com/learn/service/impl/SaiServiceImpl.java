package com.learn.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

								

import com.learn.dao.SaiDao;
import com.learn.entity.SaiEntity;
import com.learn.service.SaiService;
import com.learn.service.*;



@Service("saiService")
public class SaiServiceImpl implements SaiService {
	@Autowired
	private SaiDao saiDao;

			

			

			            @Autowired
            private  TypeService  typeService;

		

			

			

			

			

			

	

	
	@Override
	public SaiEntity queryObject(Long id){
			SaiEntity entity = saiDao.queryObject(id);

									
            if ( entity.getType() != null &&  this.typeService.queryObject(entity.getType()) != null)
                entity.setTypeEntity(this.typeService.queryObject(entity.getType()));

																		
		return entity;
	}
	
	@Override
	public List<SaiEntity> queryList(Map<String, Object> map){
        List<SaiEntity> list = saiDao.queryList(map);
        for(SaiEntity entity : list){
																					
                    if ( entity.getType() != null &&  this.typeService.queryObject(entity.getType()) != null)
                        entity.setTypeEntity(this.typeService.queryObject(entity.getType()));

																																												}
		return list;
	}
	
	@Override
	public int queryTotal(Map<String, Object> map){
		return saiDao.queryTotal(map);
	}
	
	@Override
	public void save(SaiEntity sai){
		saiDao.save(sai);
	}
	
	@Override
	public void update(SaiEntity sai){
		saiDao.update(sai);
	}
	
	@Override
	public void delete(Long id){
		saiDao.delete(id);
	}
	
	@Override
	public void deleteBatch(Long[] ids){
		saiDao.deleteBatch(ids);
	}
	
}
