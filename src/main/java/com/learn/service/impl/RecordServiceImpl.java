package com.learn.service.impl;

import com.learn.entity.TypeEntity;
import com.learn.utils.RRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.learn.service.SysUserService;


import com.learn.dao.RecordDao;
import com.learn.entity.RecordEntity;
import com.learn.service.RecordService;
import com.learn.service.*;


@Service("recordService")
public class RecordServiceImpl implements RecordService {
    @Autowired
    private RecordDao recordDao;


    @Autowired
    private SaiService saiService;


    @Autowired
    private SportService sportService;


    @Autowired
    private SysUserService sysUserService;

    @Autowired
	TypeService typeService;


    @Override
    public RecordEntity queryObject(Long id) {
        RecordEntity entity = recordDao.queryObject(id);


        if (entity.getSai() != null && this.saiService.queryObject(entity.getSai()) != null)
            entity.setSaiEntity(this.saiService.queryObject(entity.getSai()));


        if (entity.getSport() != null && this.sportService.queryObject(entity.getSport()) != null)
            entity.setSportEntity(this.sportService.queryObject(entity.getSport()));

        if (this.sysUserService.queryObject(entity.getUser()) != null)
            entity.setSysUserEntity(this.sysUserService.queryObject(entity.getUser()));


        return entity;
    }

    @Override
    public List<RecordEntity> queryList(Map<String, Object> map) {
    	//进行成绩排名处理，根据比赛类型进行排名
		//
		Map<String, Object> map1 = new HashMap<>();
		for(TypeEntity type : this.typeService.queryList(map1)){
			map1.put("type",type.getId());
			map1.put("sidx","score");
			map1.put("order",type.getSort());
			int i = 1;
            for(RecordEntity entity : this.recordDao.queryList(map1)){
                    entity.setMing(i +0d);
                    if(entity.getScore() !=null)
                        this.recordDao.update(entity);

                    i++;

                }


		}



        List<RecordEntity> list = recordDao.queryList(map);
        for (RecordEntity entity : list) {

            if (entity.getSai() != null && this.saiService.queryObject(entity.getSai()) != null)
                entity.setSaiEntity(this.saiService.queryObject(entity.getSai()));


            if (entity.getSport() != null && this.sportService.queryObject(entity.getSport()) != null)
                entity.setSportEntity(this.sportService.queryObject(entity.getSport()));

            if (this.sysUserService.queryObject(entity.getUser()) != null)
                entity.setSysUserEntity(this.sysUserService.queryObject(entity.getUser()));

        }
        return list;
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return recordDao.queryTotal(map);
    }

    @Override
    public void save(RecordEntity record) {

        //这里要开始判断限制人数
        Map<String, Object> map = new HashMap<>();
        map.put("sai",record.getSai());
        if(this.recordDao.queryList(map).size() >= this.saiService.queryObject(record.getSai()).getNum()){
            throw new RRException("该赛程人员已报满");
        }

    	record.setType(this.saiService.queryObject(record.getSai()).getType());
        record.setUser(this.sportService.queryObject(record.getSport()).getUser());
        recordDao.save(record);
    }

    @Override
    public void update(RecordEntity record) {
        record.setType(this.saiService.queryObject(record.getSai()).getType());
        record.setUser(this.sportService.queryObject(record.getSport()).getUser());
        recordDao.update(record);
    }

    @Override
    public void delete(Long id) {
        recordDao.delete(id);
    }

    @Override
    public void deleteBatch(Long[] ids) {
        recordDao.deleteBatch(ids);
    }

}
