package com.learn.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

						            import com.learn.service.SysUserService;
		

import com.learn.dao.SportDao;
import com.learn.entity.SportEntity;
import com.learn.service.SportService;
import com.learn.service.*;



@Service("sportService")
public class SportServiceImpl implements SportService {
	@Autowired
	private SportDao sportDao;

			

			

			

			

			            @Autowired
            private  DeptService  deptService;

		

			            @Autowired
            private SysUserService sysUserService;

		

			

	

	
	@Override
	public SportEntity queryObject(Long id){
			SportEntity entity = sportDao.queryObject(id);

															
            if ( entity.getDept() != null &&  this.deptService.queryObject(entity.getDept()) != null)
                entity.setDeptEntity(this.deptService.queryObject(entity.getDept()));

					            if (this.sysUserService.queryObject(entity.getUser()) != null)
                entity.setSysUserEntity(this.sysUserService.queryObject(entity.getUser()));

						
		return entity;
	}
	
	@Override
	public List<SportEntity> queryList(Map<String, Object> map){
        List<SportEntity> list = sportDao.queryList(map);
        for(SportEntity entity : list){
																																			
                    if ( entity.getDept() != null &&  this.deptService.queryObject(entity.getDept()) != null)
                        entity.setDeptEntity(this.deptService.queryObject(entity.getDept()));

											                    if (this.sysUserService.queryObject(entity.getUser()) != null)
                        entity.setSysUserEntity(this.sysUserService.queryObject(entity.getUser()));

																}
		return list;
	}
	
	@Override
	public int queryTotal(Map<String, Object> map){
		return sportDao.queryTotal(map);
	}
	
	@Override
	public void save(SportEntity sport){
		sportDao.save(sport);
	}
	
	@Override
	public void update(SportEntity sport){
		sportDao.update(sport);
	}
	
	@Override
	public void delete(Long id){
		sportDao.delete(id);
	}
	
	@Override
	public void deleteBatch(Long[] ids){
		sportDao.deleteBatch(ids);
	}
	
}
