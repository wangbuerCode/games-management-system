package com.learn.service;

import com.learn.entity.SportEntity;

import java.util.List;
import java.util.Map;

/**
 * 运动员信息
 * 
 
 * 
 */
public interface SportService {
    /**
    * 查询
	* @return
	*/
	SportEntity queryObject(Long id);

    /**
    * 查询列表
    * @return
    */
	List<SportEntity> queryList(Map<String, Object> map);

    /**
    * 查询总数
    * @return
    */
	int queryTotal(Map<String, Object> map);

    /**
    * 保存
    * @return
    */
	void save(SportEntity sport);

    /**
    * 修改
    * @return
    */
	void update(SportEntity sport);

    /**
    * 删除
    * @return
    */
	void delete(Long id);

    /**
    * 批量删除
    * @return
    */
	void deleteBatch(Long[] ids);
}
