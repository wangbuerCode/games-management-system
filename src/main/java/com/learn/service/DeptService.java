package com.learn.service;

import com.learn.entity.DeptEntity;

import java.util.List;
import java.util.Map;

/**
 * 参赛单位
 * 
 
 * 
 */
public interface DeptService {
    /**
    * 查询
	* @return
	*/
	DeptEntity queryObject(Long id);

    /**
    * 查询列表
    * @return
    */
	List<DeptEntity> queryList(Map<String, Object> map);

    /**
    * 查询总数
    * @return
    */
	int queryTotal(Map<String, Object> map);

    /**
    * 保存
    * @return
    */
	void save(DeptEntity dept);

    /**
    * 修改
    * @return
    */
	void update(DeptEntity dept);

    /**
    * 删除
    * @return
    */
	void delete(Long id);

    /**
    * 批量删除
    * @return
    */
	void deleteBatch(Long[] ids);
}
